package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectToDatabase {

	public static void main(String args[]) {
		String name="Test";
		String email="tv@gmail.com";
		String account="testAccount";
		String password="test1234";
		
		//新增會員測試
		//new ConnectToDatabase().createNewMember(name, email, account, password);
		//new ConnectToDatabase().addNewMember(name, email, account, password);
		
		//印出來，秀出會員資料測試
		//System.out.println(new ConnectToDatabase().showMemberData(17));
		
		//修改會員資料測試
		//new ConnectToDatabase().editMemberData("name", "Test", "Eric", 5);
		new ConnectToDatabase().editMemberData("name", "Test", "Ellie", 2);
		
	}
	
	
	/*新增註冊 */
	public void addNewMember(String name, String email, String account, String password) {
		Connection con= connectToDatabase();
		
		String sql="INSERT INTO customerdata(name, email) VALUES (?,?)";
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			//這段先插入名字和信箱，在表一
			ps.setString(1, name);
			ps.setString(2, email);
			ps.executeUpdate();
			
			//這段取得表一的id，條件是相同信箱
			String sql2="SELECT id FROM customerdata WHERE email= '"+email+"'";
			PreparedStatement ps2 = con.prepareStatement(sql2);
			ResultSet rs= ps2.executeQuery();
			int temp=-1;
			while(rs.next()) {
				temp=rs.getInt("id");
			}
			System.out.println(temp);
			
			//這段把取得的id(信箱為查找條件)的寫入第二個表格，並同時插入帳密
			String sql3="INSERT INTO customerdetail(id, account, password) VALUES("+temp+",?,?)";
			PreparedStatement ps3 = con.prepareStatement(sql3);
			ps3.setString(1, account);
			ps3.setString(2, password);
			ps3.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	/*更新修改*/
	public void editMemberData(String fieldName, String oldValue, String newValue, int id) {
		Connection con =  connectToDatabase();
		
		//剩下的問題是，在會員選到要更改自己會員介面時，應該會需要獲取一個ID值
		//照正常邏輯判斷，因為會員登錄時，名字可以一樣
		//name可以一樣(菜市場名或一個人擁有多個email，多個email都拿來註冊)
		//email不可以一樣(或是看公司規定，但一般來說一個email只能註冊一個帳號)
		//account不可以一樣，在註冊時應該就要檢測
		//password可以一樣，反正是個人資料
		
		/* 修改資料流程
		 * 1. 登入(login)
		 * 2. 找到個人資料頁面
		 * 3. 看是怎樣設計的資料頁面，會決定怎樣的更改資料
		 * 		(例如我寫的這個應該UI就是要設計成每一個地方都有一個變更按鈕的那種很麻煩的一次只能改一個地方)
		 * 		(或是都改完之後最下面有個更改的按鍵，那個應該要用UPDATE更新複數資料)
		 * 4. 為了不要改到其他同名的人的資料，應該在登入成功後，給一個id，然後也丟進來這個funtion裡
		 * */
		
		//看到進來的欄位名決定是哪一個表格
		//應該要用前台的設計，讓fieldName只產生某些選項(String值固定幾種這樣)
		String tableName="";
		switch(fieldName) {
			case "name":
			case "email":
				tableName="customerdata";
				break;
			case "account":
			case "password":
				tableName="customerdetail";
				break;
			default: 
				break;
		}
		
		String sql="UPDATE "+tableName+" SET "+fieldName+"=? WHERE "+fieldName+"=? AND id="+id;
		
		//要改動的值外面要加''，因為PREPAREDSTATEMENT 會認識不到'?'裡的?，會跟你說找不到?超出index
		//因為ID不能改，所以不會有需要變動int的問題
		//oldValue="'"+oldValue+"'";
		//newValue="'"+newValue+"'";
		//但其實不需要外面加''，加了反而會找不到原本的資料
		//可能PREPAREDSTATEMENT會幫我們自動加
		
		System.out.println(sql);
		System.out.println(oldValue);
		System.out.println(newValue);
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			
			
			//第1個問號是 SET fieldName=(新的值)
			ps.setString(1, newValue);
			//第2個問號是 WHERE fieldName=(舊的值)
			ps.setString(2, oldValue);
			
			ps.executeUpdate();
			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/*取會員資料*/
	public String showMemberData(int id) {
		Connection con =  connectToDatabase();
		
		//取的時候應該也有取到pass word吧，只是java印出來的時候沒印password
		//String sql="SELECT * FROM customerdata INNER JOIN customerdetail ON customerdata.id=customerdetail.id ";
		//UNION貌似是同樣名稱的欄位，從不同的表取得的意思
		//String sql = "SELECT id, name, email FROM customerdata WHERE id="+id+" UNION SELECT account FROM customerdetail WHERE id="+id+" ORDER BY id";
		//下面這樣選，可以選到想要的欄位加上WHERE可以指定條件，可是這樣就不用用string temp=temp+...這樣
		String sql="SELECT *, account FROM customerdata, customerdetail WHERE customerdata.id="+id;
		//System.out.println(sql);
		
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			
			String temp="";
			
			
			while(rs.next()) {
				temp= /*temp +*/ 	"ID: "+rs.getInt("id")+
								"\tname: "+rs.getString("name")+
								"\temail: "+rs.getString("email")+
								"\taccount: "+rs.getString("account")+"\n";
			}
			
			rs.close();
			
			return temp;
			
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	
	
	
	
	
	
	
	
	
	private Connection connectToDatabase() {
		
		String dbDriver="com.mysql.jdbc.Driver";//連線DB所需的API路徑(名稱)
		
		String account="root";//DB帳號
		String password="1234";//DB密碼
		String url="jdbc:mysql://localhost:3306/project1";//DB連線路徑
		
		try {
			Class.forName(dbDriver);
			
			Connection con= DriverManager.getConnection(url, account, password);
			return con;
		} catch (ClassNotFoundException e) {
			System.out.println("找不到連接DB的驅動程式");
			e.printStackTrace();
			return null;
		} catch (SQLException e) {
			System.out.println("連接到DB失敗了，帳號/密碼/路徑錯誤");
			e.printStackTrace();
			return null;
		}
		
		
		
	}
	
	
	
	
}



/*新增註冊*/
/*
public void createNewMember(String name, String email, String account, String password) {
	//SQL語法
	//插到第一個表(名字+信箱)
	String sqlComment="INSERT INTO customerdata(name, email) VALUES(?,?)";
	
	
	//客人資料
	//把第一個表的ID+帳+密寫到表二
	//String sqlsSelect="SELECT id FROM customerdata WHERE ?";
	
	String sqlComment2="INSERT INTO customerdetail(id, account, password) SELECT id FROM customerdata VALUES(?,?)";
	String sqlComment3="INSERT INTO customerdetail(id) SELECT id FROM customerdata";
	Connection con = connectToDatabase();
	try {
		PreparedStatement ps = con.prepareStatement(sqlComment);
		PreparedStatement ps2= con.prepareStatement(sqlComment2);
		//PreparedStatement psSelect=con.prepareStatement(sqlsSelect);
		//PreparedStatement ps3= con.prepareStatement(sqlComment3);
		
		//插入資料
		ps.setString(1, name);
		ps.setString(2, email);
		ps.executeUpdate();
		
		//ps3.executeUpdate();
		
		
		ps2.setString(1, account);
		ps2.setString(2, password);
		ps2.executeUpdate();
		
	} catch (SQLException e) {
		System.out.println("SQL語法錯誤");
		e.printStackTrace();
	}
}
*/